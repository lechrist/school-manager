// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDjGO5i0o-hOdcGNXPRi6T5SamqR2aP1nM',
    authDomain: 'gestionnaireschool.firebaseapp.com',
    databaseURL: 'https://gestionnaireschool.firebaseio.com',
    projectId: 'gestionnaireschool',
    storageBucket: 'gestionnaireschool.appspot.com',
    messagingSenderId: '706230215067'
  }
};
