import { FormControl } from '@angular/forms';

export interface ValidationResult {
    [key: string]: boolean;
}

export class EmailValidator {

    public static hasEmail(control: FormControl): ValidationResult {
        const hasValid = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$/.test(control.value);
        // console.log('Num, Upp, Low', hasNumber, hasUpper, hasLower);
        const valid = hasValid ;
        if (!valid) {
            // return what´s not valid
            return { hasEmail: true };
        }
        return null;
    }
}
