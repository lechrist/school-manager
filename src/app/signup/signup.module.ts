import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import { SignupRoutingModule } from './signup-routing.module';
import { SignupComponent } from './signup.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CompareValidator } from './shared/comparevalidator';
import { AuthService } from '../providers/authprovider/authservice';
import { AuthGuard } from '../shared';

@NgModule({
  imports: [
    CommonModule,
    SignupRoutingModule,
    ReactiveFormsModule,
    FormsModule, ShowHidePasswordModule.forRoot(),
    HttpClientModule
  ],
  declarations: [SignupComponent, CompareValidator],
})
export class SignupModule { }
