import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import { FormGroup, FormBuilder, FormControl, Validators, NgForm} from '@angular/forms';
import { User } from '../model/user.model';
import { Router } from '@angular/router';
import { AuthService } from '../providers/authprovider/authservice';
import { PasswordValidator } from './shared/passwordvalidator';
import { EmailValidator } from './shared/emailvalidator';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss'],
    animations: [routerTransition()]
})
export class SignupComponent implements OnInit {
    signupForm: FormGroup;
    user = new User();
    repeatPassword = '';
    constructor(private formBuilder: FormBuilder, private authService: AuthService ,
        public router: Router) {}

    ngOnInit() {
        this.signupForm = this.formBuilder.group({
            'email' : new FormControl(this.user.email, Validators.compose ([ Validators.required ,
              EmailValidator.hasEmail])) ,
            'password' : new FormControl(this.user.password,
               Validators.compose([Validators.required , PasswordValidator.strong
             , PasswordValidator.special , PasswordValidator.eight, PasswordValidator.number , PasswordValidator.lower ])) ,
            'repeatPassword' : new FormControl('', Validators.required)});
    }

    onRegister(value) {
        this.authService.doRegister(value)
        .then(res => {
            console.log(res);
            alert('L\'etudiant a été ajouté avec succes');
        }, (err) => {
            console.log(err);
            alert('L`\'utilisateur n`\'a pas  été du tout  enregistré');
      });
    }
}
