import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore } from 'angularfire2/firestore';

@Injectable({
  providedIn: 'root'
})

export class EtudiantService {

  constructor(
    private firestore: AngularFirestore
  ) {}

  // Service qui récupère tous les étudiants de la base de donnée
  getEtudiants(): Observable<any> {
    return  this.firestore.collection('etudiants').snapshotChanges();
  }

  // Service qui récupere un étudiant de la base de donnée
  getEtudiant(id): Observable<any> {
    return new Observable((observer) => {
    this.firestore.collection('/etudiants/').doc(id).ref.get().then((doc) => {
      const data = doc.data();
      observer.next({
        key: doc.id,
        nom: data.nom,
        prenom : data.prenom,
        email: data.email,
        classe: data.classe,
        mobile: data.mobile,
        location: data.location
      });
    });
    });
  }

  // Service qui ajoute un étudiant dans la base de donnée
  postEtudiant(etudiant) {
    return this.firestore.collection('etudiants').add(etudiant);
  }

  // Service qui met à jour un étudiant dans la base de donnée
  updateEtudiant(id: string, data): Observable<any> {
    return new Observable((observer) => {
     this.firestore.collection('etudiants').doc(id).set(data).then(() => {
      observer.next();
    });
  });
  }

  // Service qui supprime un étudiant de la base de donnée
  deleteEtudiant(id) {
    return this.firestore.collection('etudiants').doc(id).delete();
  }
  // Service qui récupère les étudiants en fonction de leur classe
    getEtudiantsByClasse(classe) {
    return new Observable((observer) => {
      this.firestore.collection('etudiants').ref.where('classe', '==', classe).onSnapshot((querySnapshot) => {
        const etudiants = [];
        querySnapshot.forEach((doc) => {
          const data = doc.data();
          etudiants.push({
            key: doc.id,
            prenom: data.prenom,
            nom: data.nom,
            email: data.email,
            classe: data.classe,
            mobile: data.mobile,
            location: data.location
          });
        });
        observer.next(etudiants);
      });
    });
  }

  // Service qui compte le nombre d'étudiants par classe
  countEtudiantByClasse(classe) {
    return this.firestore.collection('etudiants').ref.where('classe', '==', classe).get();
  }
}
