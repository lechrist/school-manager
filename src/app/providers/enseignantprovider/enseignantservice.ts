import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore } from 'angularfire2/firestore';

@Injectable({
  providedIn: 'root'
})

export class EnseignantService {

  constructor(
    private firestore: AngularFirestore
  ) {}

  // Service qui récupère toutes les enseignants de la base de donnée
  getEnseignants(): Observable<any> {
    return  this.firestore.collection('enseignants').snapshotChanges();
  }

  // Service qui récupere un enseignant en fonction de son id de la base de donnée
  getEnseignant(id): Observable<any> {
    return new Observable((observer) => {
    this.firestore.collection('/enseignants/').doc(id).ref.get().then((doc) => {
      const data = doc.data();
      observer.next({
        key: doc.id,
        nom: data.nom,
        prenom : data.prenom,
        email: data.email,
        classe: data.classe,
        mobile: data.mobile,
        location: data.location,
        course: data.course
      });
    });
    });
  }
  // Service qui récupère les enseignants en fonction de leur classe
  getEnseignantsByClasse(classe) {
    return new Observable((observer) => {
      this.firestore.collection('enseignants').ref.where('classe', '==', classe).onSnapshot((querySnapshot) => {
        const enseignants = [];
        querySnapshot.forEach((doc) => {
          const data = doc.data();
          enseignants.push({
            key: doc.id,
            nom: data.nom,
            prenom : data.prenom,
            email: data.email,
            classe: data.classe,
            mobile: data.mobile,
            location: data.location,
            course: data.course
          });
        });
        observer.next(enseignants);
      });
    });
  }

    // Service qui ajoute un enseignant dans la base de donnée
    postEnseignant(enseignant) {
        return this.firestore.collection('enseignants').add(enseignant);
    }

    // Service qui met à jour un enseignant dans la base de donnée
    updatEnseignant(id: string, data): Observable<any> {
        return new Observable((observer) => {
         this.firestore.collection('enseignants').doc(id).set(data).then(() => {
          observer.next();
        });
      });
    }


    // Service qui supprime un enseignant  de la base de donnée
    deletEnseignant(id) {
        return this.firestore.collection('enseignants').doc(id).delete();
    }
}
