import { Injectable } from '@angular/core';
import { Role } from '../../model/role';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class RoleService {
  private baseUrl = 'http://localhost:8080';

  constructor(private http: HttpClient) { }

  getRoles() {
    return new Promise(resolve => {
      this.http.get(this.baseUrl + '/api/roles/').subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  createRole(roleData: Role): Promise<Role> {
    return new Promise(resolve => {
      this.http.post(this.baseUrl + '/api/roles/', roleData)
        .toPromise()
        .then(
          res => {
            console.log(res);
            resolve();
          }
        );
    });
  }

  updateRole(roleData: Role): Promise<Role> {
    console.log('khol', roleData);
    return this.http.put(this.baseUrl + '/api/roles/' + roleData.id, roleData)
    .toPromise()
      .then((res) => res)
      .catch(this.handleError);
  }

  deleteRole(id: string): Promise<any> {
    return this.http.delete(this.baseUrl + '/api/roles/' + id)
      .toPromise()
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('Some error occured', error);
    return Promise.reject(error.message || error);
  }
}
