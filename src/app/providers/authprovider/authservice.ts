import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private afAuth: AngularFireAuth, private myRoute: Router) { }

  doLogin(value) {
    return new Promise<any>((resolve, reject) => {
      firebase.auth().signInWithEmailAndPassword(value.email, value.password)
      .then(res => {
        resolve(res);
      }, err => reject(err));
    });
  }

  doRegister(value) {
    return new Promise<any>((resolve, reject) => {
      firebase.auth().createUserWithEmailAndPassword(value.email, value.password)
      .then(res => {
        resolve(res);
      }, err => reject(err));
    });
  }

  doLogout() {
    return new Promise((resolve, reject) => {
      if (firebase.auth().currentUser) {
        this.afAuth.auth.signOut();
        resolve();
      } else {
        reject();
      }
    });
  }

doFacebookLogin() {
    return new Promise<any>((resolve, reject) => {
      const provider = new firebase.auth.FacebookAuthProvider();
      this.afAuth.auth
      .signInWithPopup(provider)
      .then(res => {
        console.log(res.user.displayName);
        resolve(res);
        localStorage.setItem('key', res.user.displayName);
      }, err => {
        console.log(err);
        reject(err);
      });
    });
}
doGoogleLogin() {
  return new Promise<any>((resolve, reject) => {
    const provider = new firebase.auth.GoogleAuthProvider();
    provider.addScope('profile');
    provider.addScope('email');
    console.log(provider.addScope('email'));
    this.afAuth.auth
    .signInWithPopup(provider)
    .then(res => {
      resolve(res);
      localStorage.setItem('key', res.user.displayName);
    });
  });
}

getToken() {
  return localStorage.getItem('key');
}

isLoggednIn() {
  return this.getToken() !== null;
}


islogout() {
  localStorage.removeItem('key');
  return this.afAuth.auth.signOut();
}

   /* public isAuthenticated(): boolean {

    const token = localStorage.getItem('key');

    // Check whether the token is expired and return
    // true or false
    return !this.jwtHelper.isTokenExpired(token);
  } */


}
