import { Injectable } from '@angular/core';
import { User } from '../../model/user.model';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class UserService {
  private baseUrl = 'http://localhost:8080';

  constructor(private http: HttpClient) { }

  getUsers() {
    return new Promise(resolve => {
      this.http.get(this.baseUrl + '/api/users/').subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }
}
