import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore } from 'angularfire2/firestore';

@Injectable({
  providedIn: 'root'
})

export class ClasseService {

  constructor(
    private firestore: AngularFirestore
  ) {}

  // Service qui récupère toutes les classes de la base de donnée
  getClasses(): Observable<any> {
    return  this.firestore.collection('classes').snapshotChanges();
  }

  // Service qui récupere une classe en fonction de son id de la base de donnée
  getClasse(id): Observable<any> {
    return new Observable((observer) => {
    this.firestore.collection('/classes/').doc(id).ref.get().then((doc) => {
      const data = doc.data();
      observer.next({
        key: doc.id,
        nom: data.nom,
        description : data.description
      });
    });
    });
  }

    // Service qui ajoute une classe dans la base de donnée
    postClasse(classe) {
        return this.firestore.collection('classes').add(classe);
    }

    // Service qui met à jour une classe dans la base de donnée
    updateClasse(id: string, data): Observable<any> {
        return new Observable((observer) => {
         this.firestore.collection('classes').doc(id).set(data).then(() => {
          observer.next();
        });
      });
    }


    // Service qui supprime une classe de la base de donnée
    deleteClasse(id) {
        return this.firestore.collection('classes').doc(id).delete();
    }
}
