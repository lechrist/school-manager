import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { AuthService } from '../providers/authprovider/authservice';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { User } from '../model/user.model';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    user =  new User();
    email = '';
    password = '';
    constructor(public router: Router, private authService: AuthService , private formbuilder: FormBuilder) {}

    ngOnInit() {
        this.loginForm = this.formbuilder.group({
            'email' : [this.email, Validators.required],
            'password' : [this.password, Validators.required],
            // 'rememberMe': this.rememberMe
        });
    }

    onLoggedin(data) {
        sessionStorage.setItem('user', data.user.email);
        // Object.assign(this.sessionService.user, sessionStorage.getItem('user'));
    }

    onLogin(value) {
        this.authService.doLogin(value)
        .then(data => {
          console.log ('user connected', data);
          this.onLoggedin(data);
          this.router.navigate(['/classes']);
        }, err => {
          console.log(err);
          alert('login or password incorrect');
          // this.errorMessage = err.message;
        });
    }
}
