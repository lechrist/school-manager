import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RoleService } from './providers/roleprovider/role.service';
import { UserService } from './providers/userprovider/user.provider';
import { AngularFireModule } from 'angularfire2';
import { environment } from '../environments/environment.prod';
import { EtudiantService } from './providers/etudiantprovider/etudiantservice';
import { AngularFirestore } from 'angularfire2/firestore';
import { AuthService } from './providers/authprovider/authservice';
import { AngularFireAuth } from 'angularfire2/auth';
// AoT requires an exported function for factories
export const createTranslateLoader = (http: HttpClient) => {
    /* for development
    return new TranslateHttpLoader(
        http,
        '/start-angular/SB-Admin-BS4-Angular-6/master/dist/assets/i18n/',
        '.json'
    ); */
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
};

@NgModule({
    imports: [
        AngularFireModule.initializeApp(environment.firebase),
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        AppRoutingModule,
        FormsModule,
        HttpModule,
    ],
    declarations: [AppComponent],
    providers: [AuthGuard, RoleService, UserService , AngularFirestore , AuthService, AngularFireAuth],
    bootstrap: [AppComponent]
})
export class AppModule {}
