import { Classe } from './classe';

export class Etudiant {
    key: string;
    prenom: string;
    nom: string;
    email: string;
    classe: Classe;
    mobile: string;
    location: string;
}
