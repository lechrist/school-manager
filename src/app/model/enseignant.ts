export class Enseignant {
    key: string;
    prenom: string;
    nom: string;
    email: string;
    classe: string;
    course: string;
    mobile: string;
    location: string;
}
