export class User {
    key: string;
    email: string;
    password: string;
    fullname: string;
    phoneNumber: string;
    dateInscription: Date;
    enabled: boolean;
    roles: {};
  }
