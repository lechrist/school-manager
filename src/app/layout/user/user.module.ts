import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserRoutingModule } from './user-routing.module';
import { PageHeaderModule } from './../../shared';
import { UserComponent } from './user.component';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

@NgModule({
    imports: [CommonModule, PageHeaderModule, UserRoutingModule, 
        FormsModule,
        HttpModule],
    declarations: [UserComponent]
})
export class UserModule { }
