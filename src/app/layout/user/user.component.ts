import { Component, OnInit } from '@angular/core';
import { Role } from '../../model/role';
import { routerTransition } from '../../router.animations';
import { UserService } from '../../providers/userprovider/user.provider';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'user-role',
  templateUrl: './user.component.html',
  animations: [routerTransition()]
})
export class UserComponent implements OnInit {
  users: any;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.getUsers();
  }

  getUsers() {
    this.userService.getUsers()
      .then(users => {
        this.users = users;
      });
  }
}
