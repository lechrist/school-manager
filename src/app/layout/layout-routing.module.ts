import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { AuthGuard } from '../shared';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'classes', pathMatch: 'prefix', canActivate: [AuthGuard] },
            { path: 'classes', loadChildren: './classes/classes.module#ClassesModule', canActivate: [AuthGuard] },
            { path: 'etudiants', loadChildren: './etudiants/etudiants.module#EtudiantsModule', canActivate: [AuthGuard] },
            { path: 'enseignants', loadChildren: './enseignants/enseignants.module#EnseignantsModule' },
            { path: 'roles', loadChildren: './role/role.module#RoleModule' },
            { path: 'users', loadChildren: './user/user.module#UserModule' },
            { path: 'charts', loadChildren: './charts/charts.module#ChartsModule' },
            { path: 'tables', loadChildren: './tables/tables.module#TablesModule' },
            { path: 'forms', loadChildren: './form/form.module#FormModule' },
            { path: 'bs-element', loadChildren: './bs-element/bs-element.module#BsElementModule' },
            { path: 'grid', loadChildren: './grid/grid.module#GridModule' },
            { path: 'components', loadChildren: './bs-component/bs-component.module#BsComponentModule' },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
