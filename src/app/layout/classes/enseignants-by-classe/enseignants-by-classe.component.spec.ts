import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnseignantsByClasseComponent } from './enseignants-by-classe.component';

describe('EnseignantsByClasseComponent', () => {
  let component: EnseignantsByClasseComponent;
  let fixture: ComponentFixture<EnseignantsByClasseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnseignantsByClasseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnseignantsByClasseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
