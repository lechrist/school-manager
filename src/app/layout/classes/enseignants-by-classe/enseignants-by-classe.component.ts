import { Component, OnInit } from '@angular/core';
import { Enseignant } from '../../../model/enseignant';
import { EnseignantService } from '../../../providers/enseignantprovider/enseignantservice';
import { ActivatedRoute } from '@angular/router';
import { routerTransition } from '../../../router.animations';

@Component({
  selector: 'app-enseignants-by-classe',
  templateUrl: './enseignants-by-classe.component.html',
  styleUrls: ['./enseignants-by-classe.component.scss'],
  animations: [routerTransition()]
})
export class EnseignantsByClasseComponent implements OnInit {
  enseignant: Enseignant = new Enseignant();
  enseignants: any;
  constructor(private enseignantService: EnseignantService,  private route: ActivatedRoute ) { }

  ngOnInit() {
    this.route.params.subscribe((param) => {
      this.enseignant.classe = param.classe;
      console.log(param.classe);
    });
    this.getEnseignantsByClasse(this.enseignant.classe);
  }

  getEnseignantsByClasse(classe) {
    this.enseignantService.getEnseignantsByClasse(classe)
    .subscribe(data => {
      this.enseignants = data;
      console.log(data);
    });
  }

}
