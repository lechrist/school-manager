import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddClasseComponent } from './add-classe/add-classe.component';
import { ListClassesComponent } from './list-classes/list-classes.component';
import { ModifClasseComponent } from './modif-classe/modif-classe.component';
import { EtudiantsByClasseComponent } from './etudiants-by-classe/etudiants-by-classe.component';
import { EnseignantsByClasseComponent } from './enseignants-by-classe/enseignants-by-classe.component';

const routes: Routes = [
  {
    path: '',
    component: ListClassesComponent
  },
  {
    path: 'add',
    component: AddClasseComponent
  },
  {
    path: 'list',
    component: ListClassesComponent
  },
  {
    path: 'list/:key',
    component: ModifClasseComponent
  },
  {
    path: 'list/etudiants/:classe',
    component: EtudiantsByClasseComponent
  },
  {
    path: 'list/enseignants/:classe',
    component: EnseignantsByClasseComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClassesRoutingModule { }
