import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, NgForm } from '@angular/forms';
import { Classe } from '../../../model/classe';
import { ActivatedRoute, Router } from '@angular/router';
import { ClasseService } from '../../../providers/classeprovider/classeservice';
import { routerTransition } from '../../../router.animations';

@Component({
  selector: 'app-modif-classe',
  templateUrl: './modif-classe.component.html',
  styleUrls: ['./modif-classe.component.scss'],
  animations: [routerTransition()]
})
export class ModifClasseComponent implements OnInit {
  classeForm: FormGroup;
  classe: Classe = new Classe();
  constructor(private classeService: ClasseService,
    private route: ActivatedRoute , private formBuilder: FormBuilder ,
    public router: Router) { }

  ngOnInit() {
    this.classeForm = this.formBuilder.group({
      'nom' : new FormControl('' , Validators.required) ,
      'description' : new FormControl('', Validators.required),
    });
    this.route.params.subscribe((param) => {
      this.classe.key = param.key;
      console.log(this.classe.key);
    });
    this.getclasse(this.classe.key);
  }

  // Récupèrer un étudiant
  getclasse(id) {
    this.classeService.getClasse(id).subscribe(data => {
      // this.id = data.key;
      this.classeForm.setValue({
        nom: data.nom,
        description : data.description,
      });
    });
  }

  // Modifier un étudiant
  update(updateForm: NgForm) {
    this.classeService.updateClasse(this.classe.key, updateForm)
    .subscribe(res => {
        alert('La classe à été bien modifiée');
        this.router.navigate(['/classes/list']);
      }, (err) => {
        console.log(err);
      }
    );
  }

}
