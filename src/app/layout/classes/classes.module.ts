import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClassesRoutingModule } from './classes-routing.module';
import { AddClasseComponent } from './add-classe/add-classe.component';
import { ListClassesComponent } from './list-classes/list-classes.component';
import { ModifClasseComponent } from './modif-classe/modif-classe.component';
import { environment } from '../../../environments/environment';
import { AngularFireModule } from 'angularfire2';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EtudiantsByClasseComponent } from './etudiants-by-classe/etudiants-by-classe.component';
import { EnseignantsByClasseComponent } from './enseignants-by-classe/enseignants-by-classe.component';

@NgModule({
  imports: [
    AngularFireModule.initializeApp(environment.firebase),
    CommonModule,
    ClassesRoutingModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  declarations: [AddClasseComponent, ListClassesComponent, ModifClasseComponent, EtudiantsByClasseComponent, EnseignantsByClasseComponent]
})
export class ClassesModule { }
