import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EtudiantsByClasseComponent } from './etudiants-by-classe.component';

describe('EtudiantsByClasseComponent', () => {
  let component: EtudiantsByClasseComponent;
  let fixture: ComponentFixture<EtudiantsByClasseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EtudiantsByClasseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtudiantsByClasseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
