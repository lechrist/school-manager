import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Etudiant } from '../../../model/etudiant';
import { EtudiantService } from '../../../providers/etudiantprovider/etudiantservice';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-etudiants-by-classe',
  templateUrl: './etudiants-by-classe.component.html',
  styleUrls: ['./etudiants-by-classe.component.scss'],
  animations: [routerTransition()]
})
export class EtudiantsByClasseComponent implements OnInit {
  etudiantForm: FormGroup;
  etudiant: Etudiant = new Etudiant();
  etudiants: any;
  effectif;
  constructor(private etudiantService: EtudiantService,  private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe((param) => {
      this.etudiant.classe = param.classe;
      console.log(param.classe);
    });
    this.getEtudiantsByClasse(this.etudiant.classe);
    this.getNbEtudiantByClasse(this.etudiant.classe);
  }

  getEtudiantsByClasse(classe) {
    this.etudiantService.getEtudiantsByClasse(classe)
    .subscribe(data => {
      this.etudiants = data;
      console.log(data);
    });
  }
  getNbEtudiantByClasse(classe) {
    this.etudiantService.countEtudiantByClasse(classe).then(res => {
      console.log(res.size);
      this.effectif = res.size;
    });
  }

}
