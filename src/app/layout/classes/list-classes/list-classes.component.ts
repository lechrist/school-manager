import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { Classe } from '../../../model/classe';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ClasseService } from '../../../providers/classeprovider/classeservice';
import 'rxjs/Rx';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-list-classes',
  templateUrl: './list-classes.component.html',
  styleUrls: ['./list-classes.component.scss'],
  animations: [routerTransition()]
})
export class ListClassesComponent implements OnInit {

  @Input() classe: Classe = new Classe();
  public documentId = null;
  public etudiantForm = new FormGroup({
    nom: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
  });
  classes: any;

  constructor(private classeService: ClasseService) { }

  ngOnInit() {
    this.getClasses();
  }

  // Récupérer la liste de toutes les classes
  getClasses() {
    this.classeService.getClasses().pipe(
      map(changes =>
        changes.map(c => ({
          key: c.payload.doc.id, ...c.payload.doc.data()
          }))
      )
    ).subscribe(classes => {
      console.log(classes);
      this.classes = classes;
    });
  }

  // Supprimer une classe
  deleteClasse(id) {
    const c = confirm('Voulez vous vraiment supprimer cette classe ?');
    if (c) {
      this.classeService.deleteClasse(id).then(() => {
        alert('Classe  supprimée!');
      }, (error) => {
        alert(error);
      });
    }
  }

}
