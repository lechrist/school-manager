import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ClasseService } from '../../../providers/classeprovider/classeservice';
import { Classe } from '../../../model/classe';
import { routerTransition } from '../../../router.animations';

@Component({
  selector: 'app-add-classe',
  templateUrl: './add-classe.component.html',
  styleUrls: ['./add-classe.component.scss'],
  animations: [routerTransition()]
})
export class AddClasseComponent implements OnInit {
  classeForm: FormGroup;
  classe = new Classe();
  constructor(private formBuilder: FormBuilder, public router: Router , private classeService: ClasseService) { }

  ngOnInit() {
    this.classeForm = this.formBuilder.group({
      'nom' : new FormControl('' , Validators.required) ,
      'description' : new FormControl('', Validators.required),
    });
  }

  // Ajouter une classe
  addClasse(form: NgForm) {
    this.classeService.postClasse(form)
      .then(res => {
          console.log(res);
          alert('La classe a été ajoutée avec succes');
          this.router.navigate(['/classes/list']);
      }, (err) => {
          console.log(err);
    });
  }

}
