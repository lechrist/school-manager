import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { EtudiantService } from '../../../providers/etudiantprovider/etudiantservice';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Etudiant } from '../../../model/etudiant';
import 'rxjs/Rx';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-list-etudiants',
  templateUrl: './list-etudiants.component.html',
  styleUrls: ['./list-etudiants.component.scss'],
  animations: [routerTransition()]
})
export class ListEtudiantsComponent implements OnInit {
  @Input() etudiant: Etudiant = new Etudiant();
  public documentId = null;
  public etudiantForm = new FormGroup({
    nom: new FormControl('', Validators.required),
    prenom: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    mobile: new FormControl('', Validators.required),
    classe: new FormControl('', Validators.required) ,
    adresse: new FormControl('', Validators.required),
  });
  etudiants: any;

  constructor(private etudiantService: EtudiantService) { }

  ngOnInit() {
    this.getEtudiants();
  }


  // Récupérer la liste de tous les étudiants
  getEtudiants() {
    this.etudiantService.getEtudiants().pipe(
      map(changes =>
        changes.map(c => ({
          key: c.payload.doc.id, ...c.payload.doc.data()
          }))
      )
    ).subscribe(etudiants => {
      console.log(etudiants);
      this.etudiants = etudiants;
    });
    // Other maner
    /*
      this.firestoreService.getCats().subscribe((catsSnapshot) => {
      this.cats = [];
      catsSnapshot.forEach((catData: any) => {
        this.cats.push({
          id: catData.payload.doc.id,
          data: catData.payload.doc.data()
        });
      })
    });
    */
  }

  // Supprimer un étudiant
  deleteEtudiant(id) {
    const c = confirm('Voulez vous vraiment supprimer cet étudiant ?');
    if (c) {
      this.etudiantService.deleteEtudiant(id).then(() => {
        alert('L"étudiant à été bel et bien  supprimé!');
      }, (error) => {
        alert(error);
      });
    }
  }

}
