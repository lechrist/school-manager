import { Component, OnInit } from '@angular/core';
import { Etudiant } from '../../../model/etudiant';
import { FormGroup, FormBuilder, FormControl, Validators, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { routerTransition } from '../../../router.animations';
import { EtudiantService } from '../../../providers/etudiantprovider/etudiantservice';
import { ClasseService } from '../../../providers/classeprovider/classeservice';
import 'rxjs/Rx';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-add-etudiant',
  templateUrl: './add-etudiant.component.html',
  styleUrls: ['./add-etudiant.component.scss'],
  animations: [routerTransition()]
})
export class AddEtudiantComponent implements OnInit {
  etudiantForm: FormGroup;
  etudiant = new Etudiant();
  classes: any;

  constructor(private formBuilder: FormBuilder, public router: Router ,
    public etudiantService: EtudiantService, private classeService: ClasseService) { }

  ngOnInit() {
    this.etudiantForm = this.formBuilder.group({
      'nom' : new FormControl('' , Validators.required) ,
      'prenom' : new FormControl('', Validators.required),
      'email' : new FormControl('', Validators.required),
      'mobile' : new FormControl('', Validators.required) ,
      'location' : new FormControl('', Validators.required),
      'classe' : new FormControl('', Validators.required),
    });
    this.getClasses();
  }

  // Ajouter un étudiant
  addEtudiant(form: NgForm) {
      this.etudiantService.postEtudiant(form)
        .then(res => {
            console.log(res);
            alert('L\'etudiant a été ajouté avec succes');
            this.router.navigate(['/etudiants/list']);
        }, (err) => {
            console.log(err);
      });
  }

  // Récupérer la liste de toutes les classes
  getClasses() {
    this.classeService.getClasses().pipe(
      map(changes =>
        changes.map(c => ({
          key: c.payload.doc.id, ...c.payload.doc.data()
          }))
      )
    ).subscribe(classes => {
      console.log(classes);
      this.classes = classes;
    });
  }

}
