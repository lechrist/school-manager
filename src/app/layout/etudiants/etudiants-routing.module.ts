import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddEtudiantComponent } from './add-etudiant/add-etudiant.component';
import { ListEtudiantsComponent } from './list-etudiants/list-etudiants.component';
import { ModifEtudiantComponent } from './modif-etudiant/modif-etudiant.component';
const routes: Routes = [
  {
    path: 'add',
    component: AddEtudiantComponent
  },
  {
    path: 'list',
    component: ListEtudiantsComponent
  },
  {
    path: 'list/:key',
    component: ModifEtudiantComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EtudiantsRoutingModule { }
