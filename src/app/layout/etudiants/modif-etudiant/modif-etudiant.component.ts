import { Component, OnInit } from '@angular/core';
import { Etudiant } from '../../../model/etudiant';
import { ActivatedRoute, Router } from '@angular/router';
import { EtudiantService } from '../../../providers/etudiantprovider/etudiantservice';
import { NgForm, FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { routerTransition } from '../../../router.animations';
import 'rxjs/Rx';
import { map } from 'rxjs/operators';
import { ClasseService } from '../../../providers/classeprovider/classeservice';

@Component({
  selector: 'app-modif-etudiant',
  templateUrl: './modif-etudiant.component.html',
  styleUrls: ['./modif-etudiant.component.scss'],
  animations: [routerTransition()]
})
export class ModifEtudiantComponent implements OnInit {
  etudiantForm: FormGroup;
  etudiant: Etudiant = new Etudiant();
  classes: any;
  constructor(private etudiantService: EtudiantService,
    private route: ActivatedRoute , private formBuilder: FormBuilder ,
     public router: Router, private classeService: ClasseService) { }

  ngOnInit() {
    this.etudiantForm = this.formBuilder.group({
      'nom' : new FormControl('' , Validators.required) ,
      'prenom' : new FormControl('', Validators.required),
      'email' : new FormControl('', Validators.required),
      'mobile' : new FormControl('', Validators.required) ,
      'location' : new FormControl('', Validators.required),
      'classe' : new FormControl('', Validators.required),
    });
    this.route.params.subscribe((param) => {
      this.etudiant.key = param.key;
      console.log(this.etudiant.key);
    });
    this.getEtudiant(this.etudiant.key);
    this.getClasses();
  }

  // Récupèrer un étudiant
  getEtudiant(id) {
    this.etudiantService.getEtudiant(id).subscribe(data => {
      // this.id = data.key;
      this.etudiantForm.setValue({
        nom: data.nom,
        prenom : data.prenom,
        email: data.email,
        classe: data.classe,
        mobile: data.mobile,
        location: data.location
      });
    });
  }

  // Modifier un étudiant
  update(updateForm: NgForm) {
    this.etudiantService.updateEtudiant(this.etudiant.key, updateForm)
    .subscribe(res => {
        alert('L\étudiant à été bien modifié');
        this.router.navigate(['/etudiants/list']);
      }, (err) => {
        console.log(err);
      }
    );
  }
  // old maner
    /*this.etudiantService.getEtudiant(this.etudiant.key).then(function(doc) {
      console.log('Document data', doc.data());
      console.log(doc.data().nom);
      this.etudiant.nom = doc.data().nom;
      // console.log(this.etud);
    });
    console.log('Données:', this.etudiant.nom);
    */

    // Récupérer la liste de toutes les classes
    getClasses() {
      this.classeService.getClasses().pipe(
        map(changes =>
          changes.map(c => ({
            key: c.payload.doc.id, ...c.payload.doc.data()
            }))
        )
      ).subscribe(classes => {
        console.log(classes);
        this.classes = classes;
      });
  }
}
