import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifEtudiantComponent } from './modif-etudiant.component';

describe('ModifEtudiantComponent', () => {
  let component: ModifEtudiantComponent;
  let fixture: ComponentFixture<ModifEtudiantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifEtudiantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifEtudiantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
