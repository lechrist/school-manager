import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EtudiantsRoutingModule } from './etudiants-routing.module';
import { AddEtudiantComponent } from './add-etudiant/add-etudiant.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListEtudiantsComponent } from './list-etudiants/list-etudiants.component';
import { AngularFireModule } from 'angularfire2';
import { environment } from '../../../environments/environment';
import { MatDialogModule, MatButtonModule } from '@angular/material';
import { NgbModalModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModifEtudiantComponent } from './modif-etudiant/modif-etudiant.component';
@NgModule({
  imports: [
    AngularFireModule.initializeApp(environment.firebase),
    CommonModule,
    EtudiantsRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule.forRoot(),
    NgbModalModule,
    MatDialogModule,
    MatButtonModule
  ],
  declarations: [AddEtudiantComponent, ListEtudiantsComponent, ModifEtudiantComponent],
  providers: []
})
export class EtudiantsModule { }
