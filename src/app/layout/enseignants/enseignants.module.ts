import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EnseignantsRoutingModule } from './enseignants-routing.module';
import { AddEnseignantComponent } from './add-enseignant/add-enseignant.component';
import { ListEnseignantsComponent } from './list-enseignants/list-enseignants.component';
import { ModifEnseignantComponent } from './modif-enseignant/modif-enseignant.component';
import { AngularFireModule } from 'angularfire2';
import { environment } from '../../../environments/environment';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    AngularFireModule.initializeApp(environment.firebase),
    CommonModule,
    EnseignantsRoutingModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  declarations: [AddEnseignantComponent, ListEnseignantsComponent, ModifEnseignantComponent]
})
export class EnseignantsModule { }
