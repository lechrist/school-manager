import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { Enseignant } from '../../../model/enseignant';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { EnseignantService } from '../../../providers/enseignantprovider/enseignantservice';
import 'rxjs/Rx';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-list-enseignants',
  templateUrl: './list-enseignants.component.html',
  styleUrls: ['./list-enseignants.component.scss'],
  animations: [routerTransition()]
})
export class ListEnseignantsComponent implements OnInit {
  enseignant: Enseignant = new Enseignant();
  public documentId = null;
  public enseignantForm = new FormGroup({
    nom: new FormControl('', Validators.required),
    prenom: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    mobile: new FormControl('', Validators.required),
    classe: new FormControl('', Validators.required) ,
    adresse: new FormControl('', Validators.required),
  });
  enseignants: any;

  constructor(private enseignantService: EnseignantService) { }

  ngOnInit() {
    this.getEnseignants();
  }


  // Récupérer la liste de tous les étudiants
  getEnseignants() {
    this.enseignantService.getEnseignants().pipe(
      map(changes =>
        changes.map(c => ({
          key: c.payload.doc.id, ...c.payload.doc.data()
          }))
      )
    ).subscribe(enseignants => {
      console.log(enseignants);
      this.enseignants = enseignants;
    });
  }

  // Supprimer un enseignant
  deletEtudiant(id) {
    const c = confirm('Voulez vous vraiment supprimer cet enseignant ?');
    if (c) {
      this.enseignantService.deletEnseignant(id).then(() => {
        alert('Enseignant  supprimé!');
      }, (error) => {
        alert(error);
      });
    }
  }

}
