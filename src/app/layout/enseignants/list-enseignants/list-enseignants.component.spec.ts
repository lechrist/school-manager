import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListEnseignantsComponent } from './list-enseignants.component';

describe('ListEnseignantsComponent', () => {
  let component: ListEnseignantsComponent;
  let fixture: ComponentFixture<ListEnseignantsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListEnseignantsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListEnseignantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
