import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddEnseignantComponent } from './add-enseignant/add-enseignant.component';
import { ListEnseignantsComponent } from './list-enseignants/list-enseignants.component';
import { ModifEnseignantComponent } from './modif-enseignant/modif-enseignant.component';

const routes: Routes = [
  {
    path: 'add',
    component: AddEnseignantComponent
  },
  {
    path: 'list',
    component: ListEnseignantsComponent
  },
  {
    path: 'list/:key',
    component: ModifEnseignantComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EnseignantsRoutingModule { }
