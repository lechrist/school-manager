import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, NgForm } from '@angular/forms';
import { Enseignant } from '../../../model/enseignant';
import { EnseignantService } from '../../../providers/enseignantprovider/enseignantservice';
import { ActivatedRoute, Router } from '@angular/router';
import { routerTransition } from '../../../router.animations';
import 'rxjs/Rx';
import { map } from 'rxjs/operators';
import { ClasseService } from '../../../providers/classeprovider/classeservice';

@Component({
  selector: 'app-modif-enseignant',
  templateUrl: './modif-enseignant.component.html',
  styleUrls: ['./modif-enseignant.component.scss'],
  animations: [routerTransition()]
})
export class ModifEnseignantComponent implements OnInit {
  enseignantForm: FormGroup;
  enseignant: Enseignant = new Enseignant();
  classes: any;
  constructor(private enseignantService: EnseignantService,
    private route: ActivatedRoute , private formBuilder: FormBuilder ,
    public router: Router, private classeService: ClasseService) { }

  ngOnInit() {
    this.enseignantForm = this.formBuilder.group({
      'nom' : new FormControl('' , Validators.required) ,
      'prenom' : new FormControl('', Validators.required),
      'email' : new FormControl('', Validators.required),
      'mobile' : new FormControl('', Validators.required) ,
      'location' : new FormControl('', Validators.required),
      'classe' : new FormControl('', Validators.required),
      'course' : new FormControl('', Validators.required),

    });
    this.route.params.subscribe((param) => {
      this.enseignant.key = param.key;
      console.log(this.enseignant.key);
    });
    this.getEnseignant(this.enseignant.key);
    this.getClasses();
  }

  // Récupèrer un étudiant
  getEnseignant(id) {
    this.enseignantService.getEnseignant(id).subscribe(data => {
      // this.id = data.key;
      this.enseignantForm.setValue({
        nom: data.nom,
        prenom : data.prenom,
        email: data.email,
        classe: data.classe,
        course: data.course,
        mobile: data.mobile,
        location: data.location
      });
    });
  }

  // Modifier un étudiant
  update(updateForm: NgForm) {
    this.enseignantService.updatEnseignant(this.enseignant.key, updateForm)
    .subscribe(res => {
        alert('L\étudiant à été bien modifié');
        this.router.navigate(['/enseignants/list']);
      }, (err) => {
        console.log(err);
      }
    );
  }

   // Récupérer la liste de toutes les classes
   getClasses() {
    this.classeService.getClasses().pipe(
      map(changes =>
        changes.map(c => ({
          key: c.payload.doc.id, ...c.payload.doc.data()
          }))
      )
    ).subscribe(classes => {
      console.log(classes);
      this.classes = classes;
    });
  }

}
