import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { FormGroup, FormBuilder, FormControl, Validators, NgForm } from '@angular/forms';
import { Enseignant } from '../../../model/enseignant';
import { Router } from '@angular/router';
import { EnseignantService } from '../../../providers/enseignantprovider/enseignantservice';
import { ClasseService } from '../../../providers/classeprovider/classeservice';
import 'rxjs/Rx';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-add-enseignant',
  templateUrl: './add-enseignant.component.html',
  styleUrls: ['./add-enseignant.component.scss'],
  animations: [routerTransition()]
})
export class AddEnseignantComponent implements OnInit {

  enseignantForm: FormGroup;
  enseignant = new Enseignant();
  classes: any;

  constructor(private formBuilder: FormBuilder, public router: Router,
    public enseignantService: EnseignantService, private classeService: ClasseService) { }

  ngOnInit() {
    this.enseignantForm = this.formBuilder.group({
      'nom' : new FormControl('' , Validators.required) ,
      'prenom' : new FormControl('', Validators.required),
      'email' : new FormControl('', Validators.required),
      'mobile' : new FormControl('', Validators.required) ,
      'location' : new FormControl('', Validators.required),
      'classe' : new FormControl('', Validators.required),
      'course' : new FormControl('', Validators.required),
    });
    this.getClasses();
  }

  // Ajouter un enseignant
  addEnseignant(form: NgForm) {
      this.enseignantService.postEnseignant(form)
        .then(res => {
            console.log(res);
            alert('L\'enseignant a été ajouté avec succes');
            this.router.navigate(['/enseignants/list']);
        }, (err) => {
            console.log(err);
      });
  }
   // Récupérer la liste de toutes les classes
   getClasses() {
    this.classeService.getClasses().pipe(
      map(changes =>
        changes.map(c => ({
          key: c.payload.doc.id, ...c.payload.doc.data()
          }))
      )
    ).subscribe(classes => {
      console.log(classes);
      this.classes = classes;
    });
  }

}
